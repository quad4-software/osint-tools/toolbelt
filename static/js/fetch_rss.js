$(document).ready(function () {
    async function handleFetchRSS() {
        const feedUrl = $('#rss-url').val();
        const full = $('#full-content').is(":checked");
        const response = await fetch('/fetch-rss/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ feed_url: feedUrl, full: full })
        });
        const resultData = await response.json();
        displayRSS(resultData.articles);
    }

    function displayRSS(articles) {
        const container = $('#result-container');
        container.empty();
        const list = $('<ul></ul>');

        articles.forEach(article => {
            const item = $('<li></li>');

            const title = $('<div></div>').text(article.title);
            item.append(title);

            const summary = $('<p></p>').html(article.summary);
            item.append(summary);

            const pubDate = $('<span></span>').text(article.pubDate);
            item.append(pubDate);

            if (article.link) {
                const link = $('<a></a>').attr('href', article.link).text('Link');
                item.append(link);
            }

            list.append(item);
        });

        container.append(list);
    }

    $('#fetch-rss-form').submit(async function (e) {
        e.preventDefault();
        await handleFetchRSS();
    });
});