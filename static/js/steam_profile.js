$(document).ready(function () {
    async function handleSteamProfile() {
        const steamId = $('#steam-id').val();
        const response = await fetch('/extract-steam-profile/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ steam_id: steamId })
        });
        const resultData = await response.json();
        displaySteamProfile(resultData);
    }

    function displaySteamProfile(data) {
        const container = $('#result-container');
        container.empty();
        container.html(`<pre>${JSON.stringify(data.profile, null, 2)}</pre>`);
        if (data.profile_screenshot) {
            container.append(`<a href="${data.profile_screenshot}" download>
                                <img src="${data.profile_screenshot}" alt="Profile Screenshot" class="thumbnail" />
                              </a>`);
        }
        if (data.workshop_screenshot) {
            container.append(`<a href="${data.workshop_screenshot}" download>
                                <img src="${data.workshop_screenshot}" alt="Workshop Screenshot" class="thumbnail" />
                              </a>`);
        }
        if (data.screenshots_screenshot) {
            container.append(`<a href="${data.screenshots_screenshot}" download>
                                <img src="${data.screenshots_screenshot}" alt="Screenshots Page" class="thumbnail" />
                              </a>`);
        }
    }

    $('#steam-profile-form').submit(async function (e) {
        e.preventDefault();
        await handleSteamProfile();
    });
});