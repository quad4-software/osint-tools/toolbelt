$(document).ready(function () {
    async function handleImageMetadata(fileInput) {
        const formData = new FormData();
        formData.append('file', fileInput.files[0]);

        const response = await fetch('/image-metadata/', {
            method: 'POST',
            body: formData
        });
        const resultData = await response.json();
        displayResult(resultData);
    }

    async function handleImageMetadataUrl(imageUrl) {
        const response = await fetch('/image-metadata-url/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ url: imageUrl })
        });
        const resultData = await response.json();
        displayResult(resultData);
    }

    function displayResult(data) {
        const container = $('#result-container');
        container.empty();
        container.html(`<pre>${JSON.stringify(data.metadata, null, 2)}</pre>`);
    }

    $('#image-metadata-form').submit(async function (e) {
        e.preventDefault();
        const fileInput = $('#image-file')[0];
        if (fileInput.files.length === 0) {
            alert('Please select an image file.');
            return;
        }
        $('#result-container').text("Extracting metadata...");
        await handleImageMetadata(fileInput);
    });

    $('#image-metadata-url-form').submit(async function (e) {
        e.preventDefault();
        const imageUrl = $('#image-url').val();
        $('#result-container').text("Extracting metadata from URL...");
        await handleImageMetadataUrl(imageUrl);
    });
});