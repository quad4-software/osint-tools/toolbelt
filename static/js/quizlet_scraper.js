$(document).ready(function () {
    async function handleQuizletScraper() {
        const quizletUrl = $('#quizlet-url').val();
        const response = await fetch('/quizlet-scrape/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ url: quizletUrl })
        });
        const resultData = await response.json();
        displayQuizletScraper(resultData.data);
    }

    function displayQuizletScraper(data) {
        const container = $('#result-container');
        container.empty();
        data.forEach(card => {
            container.append(`<div class="card">
                <h3>${card.term}</h3>
                <p>${card.definition}</p>
            </div>`);
        });
    }

    $('#quizlet-scraper-form').submit(async function (e) {
        e.preventDefault();
        await handleQuizletScraper();
    });
});