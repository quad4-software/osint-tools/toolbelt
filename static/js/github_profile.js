$(document).ready(function () {
    async function handleGithubProfile() {
        const username = $('#username').val();
        const response = await fetch('/extract-github-profile/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username })
        });
        const resultData = await response.json();
        displayGithubProfile(resultData);
    }

    function displayGithubProfile(data) {
        const container = $('#result-container');
        container.empty();
        container.append(`<h2>${data.real_name}</h2>`);
        container.append(`<p>${data.bio}</p>`);
        if (data.profile_screenshot) {
            container.append(`<a href="${data.profile_screenshot}" download>
                                <img src="${data.profile_screenshot}" alt="Profile Screenshot" class="thumbnail" />
                              </a>`);
        }

        container.append('<h3>Followers</h3>');
        data.followers.forEach(follower => {
            container.append(`<div>${follower}</div>`);
        });

        container.append('<h3>Following</h3>');
        data.following.forEach(following => {
            container.append(`<div>${following}</div>`);
        });

        container.append('<h3>Stars</h3>');
        data.stars.forEach(star => {
            container.append(`<div>${star}</div>`);
        });

        container.append('<h3>Repositories</h3>');
        data.repos.forEach(repo => {
            container.append(`<div>${repo}</div>`);
        });
    }

    $('#github-profile-form').submit(async function (e) {
        e.preventDefault();
        await handleGithubProfile();
    });
});