$(document).ready(function () {
    async function handleTranslate() {
        const text = $('#translate-text').val();
        const fromLang = $('#from-lang').val();
        const toLang = $('#to-lang').val();
        const response = await fetch('/translate/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ text, from_lang: fromLang, to_lang: toLang })
        });
        const resultData = await response.json();
        $('#result-container').html(resultData.translated_text.replace(/\n/g, '<br>'));
    }

    $('#translate-form').submit(async function (e) {
        e.preventDefault();
        await handleTranslate();
    });

    $('#copy-result-btn').click(function () {
        const textToCopy = $('#result-container').text();
        navigator.clipboard.writeText(textToCopy).then(() => {
            alert('Results copied to clipboard!');
        }).catch(err => {
            console.error('Error during copy: ', err);
        });
    });
});