$(document).ready(function () {
    async function handleDownloadVideo() {
        const videoUrl = $('#video-url').val();
        const response = await fetch('/download-video/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ url: videoUrl })
        });
        const resultData = await response.json();
        if (resultData.download_id) {
            setTimeout(() => fetchAllVideos(), 10000);
        }
    }

    async function fetchAllVideos() {
        try {
            const statusResponse = await fetch(`/check-download`);
            const statusData = await statusResponse.json();

            if (statusData.status === "completed") {
                if (statusData.video_urls) {
                    statusData.video_urls.forEach(videoUrl => displayVideo(videoUrl));
                }
            }
        } catch (error) {
            $('#result-container').text(`Error: ${error.toString()}`);
        }
    }

    function displayVideo(videoUrl) {
        const container = $('#result-container');
        const videoPlayer = $('<video></video>')
            .attr('width', '320')
            .attr('height', '240')
            .attr('controls', '');

        const source = $('<source></source>')
            .attr('src', videoUrl)
            .attr('type', 'video/mp4');

        videoPlayer.append(source);
        container.append(videoPlayer);
    }

    $('#download-video-form').submit(async function (e) {
        e.preventDefault();
        await handleDownloadVideo();
    });
});