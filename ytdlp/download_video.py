import yt_dlp
import os
import secrets

def download_video(url, config, output_dir="videos"):
    ydl_opts = config.get('ytdlp_config', {}).get('yt_dl_opts', {})

    ydl_opts['outtmpl'] = os.path.join(output_dir, '%(id)s-%(uuid)s.%(ext)s')
    ydl_opts['format'] = 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'

    if "playlist" in url or "list=" in url:
        ydl_opts['yesplaylist'] = True
    else:
        ydl_opts['noplaylist'] = True

    def generate_filename(info):
        unique_id = secrets.token_urlsafe(10)
        video_ext = info.get('ext', 'mp4')
        return f"{unique_id}.{video_ext}"

    video_paths = []
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(url, download=True)
        if 'entries' in info_dict:
            for entry in info_dict['entries']:
                video_path = generate_filename(entry)
                video_paths.append(os.path.abspath(os.path.join(output_dir, video_path)))
        else:
            video_path = generate_filename(info_dict)
            video_paths.append(os.path.abspath(os.path.join(output_dir, video_path)))

    return video_paths