from fastapi import FastAPI, BackgroundTasks, HTTPException, File, UploadFile, Request, Form
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse, JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates
from typing import Optional
from pydantic import BaseModel
from PIL import Image
import requests
import os
import uuid
import shutil
import yaml
import asyncio
import json

from translate.argos import translate_text
from ytdlp.download_video import download_video
from quizlet.quizlet_scraper import get_cards
from rss.rss_fetcher import fetch_rss_feed
from rss.fetch_fulltext import fetch_full_text
from steam.steam_extract import extract_steam_profile
from gh.github_extract import extract_github_profile

# Create necessary directories
os.makedirs("static", exist_ok=True)
os.makedirs("videos", exist_ok=True)
os.makedirs("screenshots", exist_ok=True)
os.makedirs("quizlet-dump", exist_ok=True)

tags_metadata = [
    {"name": "Translate", "description": "Translation-related operations. Convert text between languages."},
    {"name": "RSS", "description": "RSS feed fetching. Obtain and optionally fetch full article content."},
    {"name": "Video Download", "description": "Video download operations. Download videos from various sources."},
    {"name": "Image Metadata", "description": "Extract metadata from images."},
    {"name": "Steam Profile", "description": "Extract details from Steam profiles using Steam ID."},
    {"name": "GitHub Profile", "description": "Extract details from GitHub profiles using username."},
    {"name": "Quizlet Scrape", "description": "Scrape Quizlet flashcards."}
]

app = FastAPI(title="OSINT Toolbelt", openapi_tags=tags_metadata)

with open('config.yml', 'r') as f:
    config = yaml.safe_load(f)

app.add_middleware(CORSMiddleware, **config['cors_config'])

favicon_path = 'favicon.ico'
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")
app.mount("/videos", StaticFiles(directory="videos"), name="videos")
app.mount("/screenshots", StaticFiles(directory="screenshots"), name="screenshots")
app.mount("/quizlet-dump", StaticFiles(directory="quizlet-dump"), name="quizlet-dump")

class DownloadVideoRequest(BaseModel):
    url: str
    video_dir: Optional[str] = "videos"

class TranslateRequest(BaseModel):
    text: str
    from_lang: str
    to_lang: str

class FetchRSSRequest(BaseModel):
    feed_url: str
    full: Optional[bool] = False

class QuizletScrapeRequest(BaseModel):
    url: str

class ImageMetadataUrlRequest(BaseModel):
    url: str

class SteamProfileRequest(BaseModel):
    steam_id: str

class GitHubProfileRequest(BaseModel):
    username: str

downloads = {}

async def download_video_task(download_id, url, config, video_dir="videos"):
    video_paths = download_video(url, config, video_dir)
    downloads[download_id] = [os.path.basename(video_path) for video_path in video_paths]

@app.get("/")
async def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.get("/translate")
async def translate_page(request: Request):
    return templates.TemplateResponse("translate.html", {"request": request})

@app.get("/video-download")
async def video_download_page(request: Request):
    return templates.TemplateResponse("video_download.html", {"request": request})

@app.get("/rss-fetch")
async def rss_fetch_page(request: Request):
    return templates.TemplateResponse("rss_fetch.html", {"request": request})

@app.get("/ocr")
async def ocr_page(request: Request):
    return templates.TemplateResponse("ocr.html", {"request": request})

@app.get("/image-metadata")
async def image_metadata_page(request: Request):
    return templates.TemplateResponse("image_metadata.html", {"request": request})

@app.get("/steam")
async def steam_extract_page(request: Request):
    return templates.TemplateResponse("steam.html", {"request": request})

@app.get("/github")
async def github_extract_page(request: Request):
    return templates.TemplateResponse("github.html", {"request": request})

@app.get('/favicon.ico', include_in_schema=False)
async def favicon():
    return FileResponse(favicon_path)

@app.get("/quizlet")
async def quizlet_page(request: Request):
    return templates.TemplateResponse("quizlet.html", {"request": request})

@app.post("/translate/", tags=["Translate"])
def api_translate(request: TranslateRequest):
    return {"translated_text": translate_text(request.text, request.from_lang, request.to_lang, config)}

@app.post("/fetch-rss/", tags=["RSS"])
def api_fetch_rss(request: FetchRSSRequest):
    articles = fetch_rss_feed(request.feed_url)
    if request.full:
        for article in articles:
            article['content'] = fetch_full_text(article['link'])
    return {"articles": articles}

@app.post("/download-video/", tags=["Video Download"])
async def api_download_video(request: DownloadVideoRequest, background_tasks: BackgroundTasks):
    download_id = str(uuid.uuid4())
    background_tasks.add_task(download_video_task, download_id, request.url, config, request.video_dir)
    return {"message": "Video download started.", "download_id": download_id}

@app.get("/check-download")
async def check_download():
    video_dir = "videos"
    if os.path.exists(video_dir):
        video_files = os.listdir(video_dir)
        video_urls = [f"/videos/{video_file}" for video_file in video_files if os.path.isfile(os.path.join(video_dir, video_file))]
        return {"status": "completed", "video_urls": video_urls}
    return JSONResponse(content={"status": "videos folder not found"}, status_code=404)

@app.post("/quizlet-scrape/", tags=["Quizlet Scrape"], response_model=dict)
async def api_quizlet_scrape(request: QuizletScrapeRequest):
    try:
        cards = await asyncio.to_thread(get_cards, request.url)
        session_id = str(uuid.uuid4())
        filename = f"{session_id}_quizlet.json"
        filepath = os.path.join('quizlet-dump', filename)
        os.makedirs('quizlet-dump', exist_ok=True)
        with open(filepath, 'w') as jsonfile:
            json.dump(cards, jsonfile, indent=2)

        return JSONResponse(content={"data": cards, "file": filename})
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=500)

@app.post("/image-metadata/", tags=["Image Metadata"])
async def image_metadata(file: UploadFile = File(...)):
    temp_dir = "temp_images"
    os.makedirs(temp_dir, exist_ok=True)
    temp_file_path = os.path.join(temp_dir, f"temp_{uuid.uuid4().hex}.jpg")
    try:
        with open(temp_file_path, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        img = Image.open(temp_file_path)
        metadata = img.info
        return {"metadata": metadata}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    finally:
        os.remove(temp_file_path)

@app.post("/image-metadata-url/", tags=["Image Metadata"])
async def image_metadata_url(request: ImageMetadataUrlRequest):
    temp_dir = "temp_images"
    os.makedirs(temp_dir, exist_ok=True)
    temp_file_path = os.path.join(temp_dir, f"temp_{uuid.uuid4().hex}.jpg")
    try:
        response = requests.get(request.url)
        with open(temp_file_path, 'wb') as file:
            file.write(response.content)
        img = Image.open(temp_file_path)
        metadata = img.info
        return {"metadata": metadata}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    finally:
        os.remove(temp_file_path)

@app.post("/extract-steam-profile/", tags=["Steam Profile"])
def api_extract_steam_profile(request: SteamProfileRequest):
    profile_data = extract_steam_profile(request.steam_id)
    return {"profile": profile_data}

@app.post("/extract-github-profile/", tags=["GitHub Profile"])
def api_extract_github_profile(request: GitHubProfileRequest):
    profile_data = extract_github_profile(request.username)
    return {"profile": profile_data}