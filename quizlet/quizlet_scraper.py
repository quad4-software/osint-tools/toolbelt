from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import json
import os

def get_cards(url):
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options)
    try:
        driver.get(url)
        cards = WebDriverWait(driver, 20).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME, "SetPageTerms-term"))
        )
        data = []
        for card in cards:
            try:
                term_text = card.find_element(By.CSS_SELECTOR, ".TermText.notranslate.lang-en").text
                definition_texts = card.find_elements(By.CSS_SELECTOR, ".TermText.notranslate.lang-en")
                if len(definition_texts) > 1:
                    definition_text = definition_texts[1].text
                else:
                    definition_text = "Definition not found"
                data.append({"term": term_text, "definition": definition_text})
            except Exception as e:
                print(f"Error extracting card details: {e}")
    except TimeoutException:
        print("Timeout while loading the Quizlet page.")
        data = []
    finally:
        driver.quit()
    return data

if __name__ == "__main__":
    url = input("Enter Quizlet URL: ")
    cards = get_cards(url)
    os.makedirs('quizlet-dump', exist_ok=True)
    with open(os.path.join('quizlet-dump', 'cards.json'), 'w') as f:
        json.dump(cards, f, indent=2)
    print(json.dumps(cards, indent=2))