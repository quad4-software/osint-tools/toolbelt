from bs4 import BeautifulSoup
import requests
from fastapi import HTTPException
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import uuid

def extract_github_profile(username):
    base_url = f"https://github.com/{username}"
    profile_url = base_url
    followers_url = f"{base_url}?tab=followers"
    following_url = f"{base_url}?tab=following"
    stars_url = f"{base_url}?tab=stars"
    repos_url = f"{base_url}?tab=repositories"

    def get_page_soup(url):
        response = requests.get(url)
        if response.status_code != 200:
            raise HTTPException(status_code=response.status_code, detail=f"Failed to fetch page: {url}")
        return BeautifulSoup(response.text, 'html.parser')

    profile_soup = get_page_soup(profile_url)
    followers_soup = get_page_soup(followers_url)
    following_soup = get_page_soup(following_url)
    stars_soup = get_page_soup(stars_url)
    repos_soup = get_page_soup(repos_url)

    try:
        real_name = profile_soup.find('span', class_='p-name vcard-fullname d-block overflow-hidden').text.strip() if profile_soup.find('span', class_='p-name vcard-fullname d-block overflow-hidden') else ''
        bio = profile_soup.find('div', class_='p-note user-profile-bio mb-3 js-user-profile-bio f4').text.strip() if profile_soup.find('div', class_='p-note user-profile-bio mb-3 js-user-profile-bio f4') else ''
    except AttributeError:
        raise HTTPException(status_code=404, detail="Profile details not found.")

    followers = [item.find('span', class_='f4 Link--primary').text.strip() for item in followers_soup.find_all('div', class_='d-table table-fixed col-12 width-full py-4 border-bottom border-gray-light')]
    following = [item.find('span', class_='f4 Link--primary').text.strip() for item in following_soup.find_all('div', class_='d-table table-fixed col-12 width-full py-4 border-bottom border-gray-light')]
    stars = [item.find('a', class_='text-bold').text.strip() for item in stars_soup.find_all('div', class_='col-12 d-block width-full py-4 border-bottom')]
    repos = [item.find('a').text.strip() for item in repos_soup.find_all('div', class_='wb-break-all')]

    profile_data = {
        "real_name": real_name,
        "bio": bio,
        "followers": followers,
        "following": following,
        "stars": stars,
        "repos": repos
    }

    options = Options()
    options.headless = True
    driver = webdriver.Chrome(options=options)
    screenshot_dir = "screenshots/steam"
    os.makedirs(screenshot_dir, exist_ok=True)
    screenshot_path = os.path.join(screenshot_dir, f"{uuid.uuid4().hex}.png")
    driver.set_window_size(1920, 1080)
    driver.get(profile_url)
    driver.save_screenshot(screenshot_path)
    driver.quit()

    profile_data["profile_screenshot"] = screenshot_path

    return profile_data