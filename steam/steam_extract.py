from bs4 import BeautifulSoup
import requests
from fastapi import HTTPException
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import uuid

def extract_steam_profile(user_identifier):
    base_profile_url = "https://steamcommunity.com"

    custom_id_url = f"{base_profile_url}/id/{user_identifier}"
    numeric_id_url = f"{base_profile_url}/profiles/{user_identifier}"

    response = requests.get(custom_id_url)
    if response.status_code == 200:
        base_url = custom_id_url
    else:
        response = requests.get(numeric_id_url)
        if response.status_code == 200:
            base_url = numeric_id_url
        else:
            raise HTTPException(status_code=response.status_code, detail="Profile not found for Steam ID or profile not accessible.")

    workshop_url = f"{base_url}/myworkshopfiles/"
    screenshots_url = f"{base_url}/screenshots/"

    workshop_response = requests.get(workshop_url)
    screenshots_response = requests.get(screenshots_url)

    if workshop_response.status_code != 200:
        raise HTTPException(status_code=workshop_response.status_code, detail="Failed to fetch Steam workshop page.")

    if screenshots_response.status_code != 200:
        raise HTTPException(status_code=screenshots_response.status_code, detail="Failed to fetch Steam screenshots page.")

    profile_soup = BeautifulSoup(response.text, 'html.parser')
    workshop_soup = BeautifulSoup(workshop_response.text, 'html.parser')
    screenshots_soup = BeautifulSoup(screenshots_response.text, 'html.parser')

    try:
        real_name = profile_soup.find('span', class_='actual_persona_name').text
        summary = profile_soup.find('div', class_='profile_summary').text.strip() if profile_soup.find('div', class_='profile_summary') else ""
    except AttributeError:
        raise HTTPException(status_code=404, detail="Profile details not found.")

    workshop_items = [item.text.strip() for item in workshop_soup.find_all('div', class_='workshopItemTitle')]
    screenshot_items = [screenshot['src'] for screenshot in screenshots_soup.find_all('img', class_='imgWallItem')]

    profile_data = {
        "real_name": real_name,
        "summary": summary,
        "workshop_items": workshop_items,
        "screenshot_items": screenshot_items
    }

    options = Options()
    options.headless = True
    driver = webdriver.Chrome(options=options)
    screenshot_dir = "screenshots/steam"
    os.makedirs(screenshot_dir, exist_ok=True)

    profile_screenshot_path = os.path.join(screenshot_dir, f"{uuid.uuid4().hex}_profile.png")
    workshop_screenshot_path = os.path.join(screenshot_dir, f"{uuid.uuid4().hex}_workshop.png")
    screenshots_screenshot_path = os.path.join(screenshot_dir, f"{uuid.uuid4().hex}_screenshots.png")

    driver.set_window_size(1920, 1080)

    driver.get(base_url)
    driver.save_screenshot(profile_screenshot_path)

    driver.get(workshop_url)
    driver.save_screenshot(workshop_screenshot_path)

    driver.get(screenshots_url)
    driver.save_screenshot(screenshots_screenshot_path)

    driver.quit()

    profile_data["profile_screenshot"] = profile_screenshot_path
    profile_data["workshop_screenshot"] = workshop_screenshot_path
    profile_data["screenshots_screenshot"] = screenshots_screenshot_path

    return profile_data